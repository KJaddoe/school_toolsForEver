<?php
include_once("./classes/User.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ToolsForEver - Medewerkers</title>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <?php include_once( "navbar.html" ); ?>
    <div class="container">
        <!-- account aanmaken -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="padding:35px 50px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Werknemer toevoegen</h4>
                    </div>
                    <div class="modal-body" style="padding:40px 50px;">
                        <form  class="validate" role="form" method="post">
                            <button type="submit" class="btn btn-primary btn-block create_account">Account aanmaken</button>
                            <div class="form-group">
                                <label>Voorvoegsel</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9 :]/g, '');" class="form-control" name="voorvoegsel" id="voorvoegsel" placeholder="Voorvoegsel">
                            </div>
                            <div class="form-group">
                                <label>Voorletter(s)</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z]/g, '');" class="form-control" required="true" name="voorletters" id="voorletters" placeholder="Voorletter(s)">
                            </div>
                            <div class="form-group">
                                <label>Achternaam</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z]/g, '');" class="form-control" required="true" name="achternaam" id="achternaam" placeholder="Achternaam">
                            </div>
                            <div class="form-group">
                                <label>Gebruikersnaam</label>
                                <input type="email" class="form-control" required="true" name="email" id="email" placeholder="E-Mail">
                            </div>
                            <div class="form-group">
                                <label>Wachtwoord</label>
                                <input type="password" class="form-control" required="true" name="wachtwoord" id="wachtwoord" placeholder="Wachtwoord">
                            </div>
                            <div class="form-group">
                                <label>Herhaal wachtwoord</label>
                                <input type="password" class="form-control" required="true" name="herwachtwoord" id="herwachtwoord" placeholder="Herhaal wachtwoord">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block create_account">Account aanmaken</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">Annuleer</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- account bewerken -->
        <div class="modal fade" id="editUser" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="padding:35px 50px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Werknemer bewerken</h4>
                    </div>
                    <div class="modal-body" style="padding:40px 50px;">
                        <form  class="validate" role="form" action="editUser.php" method="post">
                            <button type="submit" class="btn btn-primary btn-block edit_account">Account bewerken</button>
                            <div class="form-group">
                                <label>Voorletters</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z]/g, '');" class="form-control" required="true" name="voorlettersEdit" id="voorlettersEdit" placeholder="Voorletters">
                            </div>
                            <div class="form-group">
                                <label>Voorvoegsel</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9 :]/g, '');" class="form-control" name="voorvoegselEdit" id="voorvoegselEdit" placeholder="Voorvoegsel">
                            </div>
                            <div class="form-group">
                                <label>Achternaam</label>
                                <input type="text" onkeyup="this.value=this.value.replace(/[^a-zA-Z]/g, '');"  class="form-control" required="true" name="achternaamEdit" id="achternaamEdit" placeholder="Achternaam">
                            </div>
                            <div class="form-group">
                                <label>Gebruikersnaam</label>
                                <input type="email" class="form-control" required="true" name="emailEdit" id="emailEdit" placeholder="E-Mail">
                            </div>
                            <div class="form-group">
                                <label>Wachtwoord</label>
                                <input type="password" class="form-control" name="wachtwoordEdit" id="wachtwoordEdit" placeholder="Wachtwoord">
                            </div>
                            <div class="form-group">
                                <label>Wachtwoord herhalen</label>
                                <input type="password" class="form-control" name="herwachtwoordEdit" id="herwachtwoordEdit" placeholder="wachtwoord herhalen">
                            </div>
                            <input type="hidden" id="id" name="id">
                            <button type="submit" class="btn btn-primary btn-block edit_account">Account bewerken</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">Annuleer</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($_SESSION['user_role'] === "buitendienst") {
            echo '<h1>Uw rol is buitendienst</h1>';
            echo '<h1>U heeft geen toegang tot de gegevens van deze pagina</h1>';
            echo '
                    <script>
                        alert("u heeft geen toegang tot deze pagina. u word doorgestuurd naar de home pagina");
                        window.location.href = "/";
                    </script>
                 ';
        } else {
        ?>
            <!-- werknemer overzicht -->
            <input type="text" onkeyup="filterTable()" class="form-control" required="true" id="search" placeholder="Zoek">
            <br>
            <button type="button" class="btn btn-primary pull-right add" name="button">Voeg werknemer toe</button>
            <button type="button" class="btn btn-primary pull-right delete" name="button">Verwijder account</button>
            <table id="myTable" class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Werknemerscode</th>
                        <th>Voorletter</th>
                        <th>Voorvoegsel</th>
                        <th>Achternaam</th>
                        <th>Gebruikersnaam</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      printAccount();

                      addAccount();
                    ?>
                </tbody>
            </table>

            <button type="button" class="btn btn-primary pull-right add" name="button">Voeg werknemer toe</button>
            <button type="button" class="btn btn-primary pull-right delete" name="button">Verwijder account</button>
            <br><br>
            <hr>
            <?php } ?>
        <footer>
            <p>&copy; ToolsForEver 2017</p>
        </footer>
    </div>
    <script src="js/jquery-3.1.1.min.js" charset="utf-8"></script>
    <script src="js/bootstrap.min.js" charset="utf-8"></script>
    <script src="js/javascript.js" charset="utf-8"></script>
</body>

</html>

<?php
//With this function the data is displayed in a table.
function printAccount() {
    global $user;

    $result = $user->getAllUsers();
    
    $i = 0;
    foreach($result as $value) {
        echo "<tr ondblclick='editAccount(this)'><td>";
        echo "<input id='account$i'class='deleteAccount' type='checkbox'>";
        echo "</td><td>";
        echo $value['Medewerkerscode'];
        echo "</td><td>";
        echo $value['Voorletters'];
        echo "</td><td>";
        echo $value['Voorvoegsels'];
        echo "</td><td>";
        echo $value['Achternaam'];
        echo "</td><td>";
        echo $value['Email'];
        echo "</td></tr>";
        $i++;
    }
}

//with this function an account can be added to the database.
function addAccount() {
    if(isset($_POST) && !empty($_POST)) {
        global $user;

        $voorletters = $_POST['voorletters'];
        $voorvoegsel = $_POST['voorvoegsel'];
        $achternaam = $_POST['achternaam'];
        $email = $_POST['email'];
        $wachtwoord = $_POST['wachtwoord'];
        $herwachtwoord = $_POST['herwachtwoord'];

        $user->register($voorvoegsel, $voorletters, $achternaam, $email, $wachtwoord, $herwachtwoord);

    }
}
