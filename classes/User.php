<?php
session_start;
//$_SESSION['user_role'] = 'buitendienst'; //hardcoded user_role, because there is no login functionality
include_once 'dbConnection.php';

class User {
    private $db;

    public function __construct() {
        $this->db = new Connection();
        $this->db = $this->db->dbConnect();
    }

    public function Login($email, $pass) {
        if(!empty($email) && !empty($pass)) {
            $sql = "SELECT ID, userName, userMail, userPass FROM user WHERE userMail = :mail";
            $statement = $this->db->prepare($sql);

            $statement->bindParam(':mail', $email);
            $statement->execute();
            $results = $statement->fetch(PDO::FETCH_ASSOC);

            if(count($results) > 0 && password_verify($pass, $results['userPass'])) {
                $_SESSION['user_role'] = $results['role'];
                header("Refresh: 1; url=/user");
                echo 'succesfully logged in';
            }
            else {
                echo 'Could not log in';
            }
        }
    }


    //Get all users
    public function getAllUsers() {
        $sql = "SELECT * FROM Medewerker";

        $statement = $this->db->prepare($sql);

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    //Get one user
    public function getUser($id) {
        $sql = "SELECT * FROM Medewerker WHERE Medewerkerscode = :id";

        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    //Edit existing user
    public function updateUser($id, $voorvoegsel, $voorletters, $achternaam, $email, $password, $repassword) {
        $sql = "UPDATE `Medewerker` SET `Voorletters`= :voorletters,`Voorvoegsels`= :voorvoegsel,`Achternaam`= :achternaam,`Email`= :email,`Wachtwoord`= :pass WHERE `Medewerkerscode` = :id";
        $statement = $this->db->prepare($sql);

        $statement->bindParam(':id', $id);
        $statement->bindParam(':voorvoegsel', $voorvoegsel);
        $statement->bindParam(':voorletters', $voorletters);
        $statement->bindParam(':achternaam', $achternaam);
        $statement->bindParam(':email', $email);
        $statement->bindParam(':pass', password_hash($password, PASSWORD_BCRYPT));


        if ($statement->execute()) {
            echo '<script>alert("Employee has been updated")</script>';
            echo '<script>window.location.replace("/accounts.php")</script>';
        }
        else{
            echo '<script>alert("Could not update employee")</script>';
            echo '<script>window.location.replace("/accounts.php")</script>';
        }
    }

    //Delete user from datebase
    public function deleteUser($id) {
        $message = '';
        $sql = "DELETE FROM Medewerker WHERE `Medewerkerscode` = :id";

        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);

        if ($statement->execute()) {
            $message = 'Account has been deleted';
        }
        else{
            $message = 'Something went wrong and while deleting this account';
        }
        echo json_encode($message);
    }

    //Register new user
    public function register($voorvoegsel, $voorletters, $achternaam, $email, $password, $repassword){
        if(!empty($voorvoegsel) && !empty($voorletters) && !empty($achternaam) && !empty($email) && !empty($password) && !empty($repassword)){
            if($password === $repassword){
                $sql = "INSERT INTO `Medewerker`(`Voorletters`, `Voorvoegsels`, `Achternaam`, `Email`, `Wachtwoord`) VALUES (:voorletters, :voorvoegsel, :achternaam, :email, :pass)";
                $statement = $this->db->prepare($sql);

                $statement->bindParam(':voorvoegsel', $voorvoegsel);
                $statement->bindParam(':voorletters', $voorletters);
                $statement->bindParam(':achternaam', $achternaam);
                $statement->bindParam(':email', $email);
                $statement->bindParam(':pass', password_hash($password, PASSWORD_BCRYPT));


                if ($statement->execute()) {
                    echo 'Your account has been created';
                    echo '<script>window.location.replace("/accounts.php")</script>';
                }
                else{
                    echo 'Something went wrong and while creating this account (check if email does is not used by another user)';
                }
            } else {
                echo 'The passwords do not match';
            }
        } else {
            echo 'Please fill out all fields';
        }
    }
}

$user = new User();

?>
