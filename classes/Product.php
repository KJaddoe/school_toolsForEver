<?php
include_once 'dbConnection.php';

class Product {
    private $db;

    public function __construct() {
        $this->db = new Connection();
        $this->db = $this->db->dbConnect();
    }

    //Get all products
    public function getAllProducts() {
        $sql = "
                SELECT Artikel.Productcode, Artikel.Product, Artikel.Type, Fabriek.Fabriek, Voorraad.Aantal, Locatie.Locatie,
                REPLACE(round(Artikel.Inkoopprijs,2),'.',',') as Inkoopprijs,
                REPLACE(round(Artikel.Verkoopprijs,2),'.',',') as Verkoopprijs
                FROM Artikel
                INNER JOIN Fabriek ON Artikel.Fabriekcode = Fabriek.fabriekcode
                INNER JOIN Voorraad ON Artikel.Productcode = Voorraad.Productcode
                INNER JOIN Locatie ON Voorraad.Locatiecode = Locatie.Locatiecode
                ";

        $statement = $this->db->prepare($sql);

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
}

$product = new Product();

?>
