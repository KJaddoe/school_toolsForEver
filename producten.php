<?php
 include_once ("./classes/Product.php");
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ToolsForEver - Producten</title>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">


</head>

<body>
    <?php include_once( "navbar.html" ); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>

                    <input type="text" onkeyup="filterTable()" class="form-control" required="true" id="search" placeholder="Zoek">
                    <table id="myTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Productcode</th>
                                <th>Locatie</th>
                                <th>Product</th>
                                <th>Type</th>
                                <th>Fabriek</th>
                                <th>Aantal</th>
                                <th>Inkoopprijs</th>
                                <th>Verkoopprijs</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              printProducts();
                            ?>
                        </tbody>
                    </table>
                    <hr>

                    <footer>
                        <p>&copy; ToolsForEver 2017</p>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/javascript.js"></script>
</body>

</html>

<?php

//used to print the products in a table.
function printProducts()
{
  global $product;

  $result = $product->getAllProducts();

  foreach($result as $value)
  {
    echo "<tr><td>";
    echo $value['Productcode'];
    echo "</td><td>";
    echo $value['Locatie'];
    echo "</td><td>";
    echo $value['Product'];
    echo "</td><td>";
    echo $value['Type'];
    echo "</td><td>";
    echo $value['Fabriek'];
    echo "</td><td>";
    echo $value['Aantal'];
    echo "</td><td>";
    echo $value['Inkoopprijs'];
    echo "</td><td>";
    echo $value['Verkoopprijs'];
    echo "</td></tr>";
  }
}
?>
