<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ToolsForEver - Contact</title>
	<link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<?php include_once( "navbar.html" ); ?>
	<div id="page-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<br>
					<div class="row-fluid">
						<div class="span8">
							<iframe frameborder="0" height="350" marginheight="0" marginwidth="0" scrolling="no" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d38967.19373328317!2d5.190404806841078!3d52.380399325642266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6171b95a98ca3%3A0x8466b5686f9e186e!2sAlmere+Stad%2C+Almere!5e0!3m2!1snl!2snl!4v1493643301938" width="100%"></iframe>
						</div>
						<div class="col-lg-4">
							<h2>ToolsForEver</h2>
							<address>
								<strong>Hoofdkantoor</strong><br>
                                Bezoekadres:<br>
                                    ToolsForEver<br>
                                    Enter 36-42<br>
                                    EINDHOVEN<br><br>

                                Postadres:<br>
                                    ToolsForEver<br>
                                    Postbus 12345<br>
                                    5600 VM EINDHOVEN<br><br>

								<abbr title="Email">Email:</abbr> info@toolsforever.nl<br>
								<abbr title="Phone">Telefoon:</abbr> 040 987 65 00
								<abbr title="Fax">Fax:</abbr> 040 987 65 99
							</address>
						</div>
						<div class="col-lg-4">
							<h2>ToolsForEver</h2>
							<address>
								<strong>Kantoor 2</strong><br>
								Voorraadweg 22<br>
								Flevoland<br>
								Almere<br>
								Nederland<br>
								1342LL<br>
								<abbr title="Email">E:</abbr> support@toolsforever.nl<br>
								<abbr title="Phone">P:</abbr> 06 123 456 78
							</address>
						</div>
						<div class="col-lg-4">
							<h2>ToolsForEver</h2>
							<address>
								<strong>Kantoor 3</strong><br>
								Voorraadweg 22<br>
								Flevoland<br>
								Almere<br>
								Nederland<br>
								1342LL<br>
								<abbr title="Email">E:</abbr> support@toolsforever.nl<br>
								<abbr title="Phone">P:</abbr> 06 123 456 78
							</address>
						</div>
					</div>
					<hr>
				</div>
				<footer>
					<p>&copy; ToolsForEver 2017</p>
				</footer>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.1.1.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script>
</body>
</html>
