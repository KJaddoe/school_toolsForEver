<?php
include_once("./classes/User.php");

if(isset($_POST) && !empty($_POST)) {
    global $user;

    $id = $_POST['id'];
    $voorletters = $_POST['voorlettersEdit'];
    $voorvoegsel = $_POST['voorvoegselEdit'];
    $achternaam = $_POST['achternaamEdit'];
    $email = $_POST['emailEdit'];
    $wachtwoord = $_POST['wachtwoordEdit'];
    $wachtherhaal = $_POST['herwachtwoordEdit'];

    $user->updateUser($id, $voorvoegsel, $voorletters, $achternaam, $email, $wachtwoord, $herwachtwoord);
}
?>
