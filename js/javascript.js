//Used for deleting an account
var i = 0;
$(document).ready(function() {
    passwordValidation();

    //open modal
    $(".add").click(function() {
        $("#myModal").modal();
    });

    $('.delete').click(function() {
        $('.deleteAccount').each(function() {
            if (document.getElementById('account' + i).checked) {
                var clickBtnValue = $(this).parent().parent().children()[1].innerHTML;
                var ajaxurl = 'delete.php',
                    data = {
                        'action': clickBtnValue
                    };
                $.post(ajaxurl, data, function(response) {
                    alert(response);
                    location.reload();
                });
            }
            i++;
        });
        i = 0;
    });


});

function passwordValidation() {
    //Adding a validation to password and password repeat
    $('.edit_account').click(function() {
        if ($("#wachtwoordEdit").val() != $("#herwachtwoordEdit").val()) {
            document.getElementById("herwachtwoordEdit").setCustomValidity('Wachtwoord komt niet overeen.');
        } else {
            document.getElementById("herwachtwoordEdit").setCustomValidity('');
        }
    });
    $('.create_account').click(function() {
        if ($("#wachtwoord").val() != $("#herwachtwoord").val()) {
            document.getElementById("herwachtwoord").setCustomValidity('Wachtwoord komt niet overeen.');
        } else {
            document.getElementById("herwachtwoord").setCustomValidity('');
        }
    });
}

//function to edit account.
function editAccount(element) {

    element = $(element).children();

    var id = element[1].innerHTML;
    var voorletter = element[2].innerHTML;
    var voorvoegsel = element[3].innerHTML;
    var achternaam = element[4].innerHTML;
    var gebruikersnaam = element[5].innerHTML;

    $("#editUser").modal();
    $("#voorlettersEdit").val(voorletter);
    $("#voorvoegselEdit").val(voorvoegsel);
    $("#achternaamEdit").val(achternaam);
    $("#emailEdit").val(gebruikersnaam);
    $("#editUser #id").val(id);
}


//function to search in tables. (location and product)
function filterTable() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.getElementById("search");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    td2 = tr[i].getElementsByTagName("td")[2];
    if (td || td2) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1  ) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
